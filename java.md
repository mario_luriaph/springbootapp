 # mein Java Notizrn
 ## Datetypen:
 ### primitiven Datentypen:
 * boolean: Wahr oder falsch
 * char: hier kann ein zeichen gespeichert werden
 ### nicht Primitiven Datentypen
 * String: man kann hier Texte speichern. eine ankettung von mehrere char-Typ
 * **final**-> bedeutet dass den wert von Programierstruktur nicht mehr geändert werden kann
 
 ## Parsing
 umwandeln von einem Datentypn in einer anderen Datentypen.
 zumbeispiel: 
 ```
String alter = "30";
int alterInt = Integer.parseInt(alter);
```

## Typecasting
### Expliziete Typeumwandlung:
```
int i1 = (int) d1;
```
### impliziete Typeumwandlung:
```
double d1 = i1;
double d2 = i2;
double ergebnisse = ...
```
## methoden
`Zuhriffsmodifizierer Rückgabetype Name (Parameter) {Code}`
* Zuhriffsmodifizierer: gibt an, von wo die methode aufgerufen werden kann. (22)
* Rückgabewert: was wird zuruckgegeben (22)

### Static
einer Programmierstruktur, der static ist, ist nicht an einem Objekt von einem klasse gebunden.
man ändert der den Wert von einer Statischen Variablen in einem Objekt einer Klasse, ändert sich die wert in
allenanderesn Objeke auch.
more: https://javabeginners.de/Grundlagen/Modifikatoren/static.php
https://www.youtube.com/watch?v=GTTDS4Nqy1c&ab_channel=PanjuTorials

## input
```
String nutzerEingabe;
Scanner eingabe = new Scanner(System.in);
System.out.println("gib irgendwas ein");
nutzerEingabe=eingabe.nextLine();
System.out.println(nutzerEingabe);
``` 

## Arrays
ein Array ist ein Objekt mit mehere Werte
```z.B: int[] numbers = new int[4];``` 
```z.B: int[] numbers = new int[] {5, 0, 2, 3};``` 

### auf elemente zugreifen:
zum Beispiel:
```
System.out.println(number[0]);
=> 5
```

### werte zuweisen:
```
number[0] = 100;
System.out.println(number[0]);
=> 100
```
bei Arrays können von jedem Datentyp erstellt werden.

## foreach
Beispiel:
```  
String[] Freunde = new String[] {"Fuad", "dan", "viktor", "kire_khar" };
   
           for (String freund : Freunde) {
               System.out.println(freund);
           }
```

## in Java ist "passed by reference"
* Wenn ein Parameter per Referenz übergeben wird, verwenden der Aufrufer und der Aufgerufene dieselbe Variable für den Parameter. Wenn der Aufrufer die Parametervariable ändert, ist die Wirkung für die Variable des Aufrufers sichtbar.
* Wenn ein Parameter als Value übergeben wird, haben der Aufrufer und der Aufgerufene zwei unabhängige Variablen mit dem gleichen Wert. Wenn der Aufruf die Parametervariable ändert, ist die Auswirkung für den Aufrufer nicht sichtbar.

meiner meinung nach ist java **Pass bei Refrence**. Beispiel in den folgenden Code ändert sich den Array.
```
package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World");

        int[] noten = {1, 2, 3, 1, 1, 2, 3, 1, 2};

        for (int not : noten) {
            System.out.println(not);
        }
        System.out.println("______");
        System.out.println("vor dem Methode");
        System.out.println("______");
        notenKorrektor(noten);
        System.out.println("______");
        System.out.println("nach dem Methode");
        System.out.println("______");

        for (int not : noten) {
            System.out.println(not);
        }


    }
    static int notenKorrektor(int note[]) {
        for (int i = 0; i < note.length; i++) {
            note[i] = note[i] + 1;
            System.out.println(note[i]);
        }

        return 0;
    }

}

```

## Arraylist 
das ist eine art dynamische array und ist eigentlich eine Class und hat einige hilfsmethodenwie:
.add();
.get();
.remove();
``ArrayList<String> cars = new ArrayList<String>(); // Create an ArrayList object```

### Collections.sort(<Arraylist>);
diese klasse sortiert zahlen oder zeichenfolgen

## Objektorientierung
### klassen
man kann eine Klasse erstellen und es verwenden.
### zugriffsmodifikatoren
**Private** : das Programmierkonstrukt ist nur innerhalb der klasse erreichbar und von einer anderen klasse kann man den nicht erreichen.

**Public:** das Programmierkonstrukt ist von ausserhalb erreichbar

### Konstruktor

```
package com.company;

public class Mensch {
    int alter;
    String name;
    String nachname;
    String haarfarbe;

    public Mensch (int alter, String name, String nachname, String haarfarbe ){
        this.alter = alter;
        this.name = name;
        this.nachname = nachname;
        this.haarfarbe = haarfarbe;
    }

    public void vorstellen() {
        System.out.println("Hallo ich bin: " + name + " " + nachname + " meine haarfarbe ist: " + haarfarbe + " und ich bin: " + alter + " Jahre alt.");
    }
}
```

### setter und getter
am besten automatisch generieren: mit Command+N 

### vererbung 
DRY (dont repeat yourself). 
um eine Klasse zu erben benutzt man keyword `extends`
"**this**" keyword bedeutet dass die Programmierkonstrukt von der Objekt in der klasse

"**Super**" bedeutet, dass etwas in ElternKlassen 


### polymorphi
https://panjutorials.de/tutorials/java-tutorial-programmieren-lernen-fuer-anfaenger/lektionen/polymorphie-in-java/

### exeptions
mit Try catch können wir die möglichen Fehler abfangen:
Beispiel:
```

```

mit Throw können wir selbst exeptions entwefren:
Beispiel:
```
if(age < 18){
    throw new ArithmeticExeption("Acces denied - you are too Young");
}
```
### in datei schreiben 
Udamy kurs 94.


---
# Spring boot
entwedr von hier: https://start.spring.io/ oder über intellij new>projekt>springBoot einen Projekt mit abhängigkeiten starten

Services und Controller 