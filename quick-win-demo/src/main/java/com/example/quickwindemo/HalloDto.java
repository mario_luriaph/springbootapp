package com.example.quickwindemo;

public class HalloDto {
    private String text;

    public HalloDto(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
