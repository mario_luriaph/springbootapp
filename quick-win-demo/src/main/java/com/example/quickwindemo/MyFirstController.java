package com.example.quickwindemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyFirstController {
    @Autowired
    private MyfirstService myfirstService;
    @GetMapping
    public HalloDto hello(@RequestParam(required = false)String user){
        return myfirstService.hello(user);
    }

}
