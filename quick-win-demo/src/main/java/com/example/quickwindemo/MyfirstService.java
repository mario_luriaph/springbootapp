package com.example.quickwindemo;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;


@Service
public class MyfirstService {
    public MyfirstService(){
        System.out.println("MyfirstService Constructor called");
    }
    @EventListener(classes = ApplicationReadyEvent.class)
    public void init(){
        System.out.println("MyfirstApp is ready");
    }


    static int counter=0;
    public HalloDto hello(String user){
        counter++;
        return new HalloDto(String.format("Ich bin " + user + ", this App ist up and Runnuíg: " + counter));
    }
}
